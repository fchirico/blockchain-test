package it.fc.blockchain.utils;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Cypher {

    public static byte[] hash256(String pas) throws NoSuchAlgorithmException {
        java.security.MessageDigest d = null;
        d = java.security.MessageDigest.getInstance("SHA-256");
        d.reset();
        d.update(pas.getBytes());
        return d.digest();
    }

    public static String hash2Base64(String pas) throws NoSuchAlgorithmException {
        return Base64.getEncoder().encodeToString(hash256(pas));
    }
    public static byte[] hash2Base64Byte(String pas) throws NoSuchAlgorithmException {
        return Base64.getEncoder().encode(hash256(pas));
    }
   
    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.out.println(hash2Base64("CiaoATutti28"));
    }

}