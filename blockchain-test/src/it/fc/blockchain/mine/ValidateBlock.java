package it.fc.blockchain.mine;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Random;

import it.fc.blockchain.model.Block;
import it.fc.blockchain.utils.Cypher;
public class ValidateBlock {

  public static String seal = "00";
  public static String givenRandomString() {

   int leftLimit = 97; // letter 'a'
  int rightLimit = 122; // letter 'z'
  int targetStringLength = 5;
  Random random = new Random();
  StringBuilder buffer = new StringBuilder(targetStringLength);

  for (int i = 0; i < targetStringLength; i++) {

   int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));

   buffer.append((char) randomLimitedInt);

  }
  String generatedString = buffer.toString();
  return generatedString;
 }


  public static String getSeal() {
  return seal;
 }


  public void setSeal(String seal) {
  this.seal = seal;
 }


  /**

  * Dato un blocco cerca una codifica SHA256 in BASE64 che cominci con il numero

  * di "Difficulty" pari alla lunghezza dei caratteri definiti in "Seal". Appena

  * la trova valorizza il timestamp del nodo

  * 

  * @param b

  * @return

  * @throws NoSuchAlgorithmException

  */

 public static boolean validateBlock(Block b) throws NoSuchAlgorithmException {

  boolean ret = false;
  String validHash = "11";
  System.out.println("Inizio mining"+ new Timestamp(System.currentTimeMillis()));
  String tempNonce = null;
  while (!validHash.substring(0, getSeal().length()).equals(getSeal())) {
   tempNonce = givenRandomString();
   b.setNonce(tempNonce);
   validHash = Cypher.hash2Base64(b.toString());
   System.out.println(validHash);

  }

  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
  b.setTimestamp(String.valueOf(timestamp.getTime()));
  ret = true;
  System.out.println("Fine mining"+ new Timestamp(System.currentTimeMillis()));
  b.setHash(validHash);
  b.setNonce(tempNonce);

  return ret;


  }


  public static void main(String[] args) throws NoSuchAlgorithmException {

   Block b = new Block();
  b.setDati("Ciao a tutti quanti");
  b.setId("1");
  b.setNonce("0000");
  if (validateBlock(b)) {
   System.out.println("Id = " + b.getId());
   System.out.println("Nonce = " + b.getNonce());
   System.out.println("Dati = " + b.getDati());
   System.out.println("Timestamp = " + b.getTimestamp());
   System.out.println("Blocco precedente = " + b.getPrecedente());
   System.out.println("Hash = " + b.getHash());

  }
 }

}