package it.fc.blockchain.model;
import java.security.NoSuchAlgorithmException;

import it.fc.blockchain.utils.Cypher;  
public class Block {  
 String id = "";  
 String nonce = "";  
 String dati = "";  
 String timestamp = "";  
 String precedente = "";  
 String hash = "";  
 public String getId() {  
 return id;  
 }  
 public void setId(String id) {  
 this.id = id;  
 }  
 public String getNonce() {  
 return nonce;  
 }  
 public void setNonce(String nonce) {  
 this.nonce = nonce;  
 }  
 public String getDati() {  
 return dati;  
 }  
 public void setDati(String dati) {  
 this.dati = dati;  
 }  
 public String getTimestamp() {  
 return timestamp;  
 }  
 public void setTimestamp(String timestamp) {  
 this.timestamp = timestamp;  
 }  
 public String getPrecedente() {  
 return precedente;  
 }  
 public void setPrecedente(String precedente) {  
 this.precedente = precedente;  
 }  
 public String getHash() throws NoSuchAlgorithmException {  
 return Cypher.hash2Base64(this.toString());  
 }  
 public void setHash(String hash) {  
 this.hash = hash;  
 }  
 @Override  
 public String toString(){  
 return this.nonce+this.id+this.dati+this.precedente;  
 }  
}  